package com.daparcoc.laboratorio43_aplicacion1;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btnSinHilos;
    private Button btnHilo;
    private Button btnAsyncTask;
    private Button btnCancelar;
    private Button btnAsyncDialog;
    private ProgressBar pbarProgreso;
    private ProgressDialog pDialog;
    private MiTareaAsincrona tarea1;
    private MiTareaAsincronaDialog tarea2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnSinHilos = (Button) findViewById(R.id.btnSinHilos);
        btnHilo = (Button) findViewById(R.id.btnHilo);
        btnAsyncTask = (Button) findViewById(R.id.btnAsyncTask);
        btnCancelar = (Button) findViewById(R.id.btnCancelar);
        btnAsyncDialog = (Button) findViewById(R.id.btnAsyncDialog);
        pbarProgreso = (ProgressBar) findViewById(R.id.pbarProgreso);
        tarea1 = new MiTareaAsincrona();

        btnSinHilos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pbarProgreso.setMax(100); //valor máximo de ProgressBar
                pbarProgreso.setProgress(0); //Empieza vacía ProgressBar
                for (int i = 1; i <= 10; i++) {
                    tareaLarga();
                    pbarProgreso.incrementProgressBy(10); //incremento
                }
                Toast.makeText(MainActivity.this, "Tarea Finalizada!",
                        Toast.LENGTH_SHORT).show();
            }
        });

        btnAsyncDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pDialog = new ProgressDialog(MainActivity.this);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setMessage("Procesando...");
                pDialog.setCancelable(true);
                pDialog.setMax(100);
                tarea2 = new MiTareaAsincronaDialog();
                tarea2.execute();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void tareaLarga() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {}
    }

    public void onClickBtnHilo(View view) {
        new Thread(new Runnable() {
            public void run() {
                pbarProgreso.post(new Runnable() {
                    public void run() {
                        pbarProgreso.setProgress(0);
                    }
                });
                for (int i = 1; i <= 10; i++) {
                    tareaLarga();
                    pbarProgreso.post(new Runnable() {
                       public void run() {
                           pbarProgreso.incrementProgressBy(10);
                       }
                    });
                }
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, "Tarea finalizada!",
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).start();
    }

    public void onClickBtnAsyncTask(View view) {
        new MiTareaAsincrona().execute();
    }

    public void onClickBtnCancelar(View view) {
        tarea1.onCancelled();
    }

    private class MiTareaAsincrona extends AsyncTask<Void, Integer, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            for (int i = 1; i <= 10; i++) {
                tareaLarga();
                publishProgress(i*10);
                if(isCancelled())
                    break;
            }
            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int progreso = values[0].intValue();
            pbarProgreso.setProgress(progreso);
        }

        @Override
        protected void onPreExecute() {
            pbarProgreso.setMax(100);
            pbarProgreso.setProgress(0);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result)
                Toast.makeText(MainActivity.this, "Tarea finalizada!",
                        Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onCancelled() {
            this.cancel(true);
            Toast.makeText(MainActivity.this, "Tarea cancelada!",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private class MiTareaAsincronaDialog extends AsyncTask<Void,Integer,Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            for (int i = 1; i <= 10; i++) {
                tareaLarga();
                publishProgress(i*10);
                if(isCancelled())
                    break;
            }
            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int progreso = values[0].intValue();
            pDialog.setProgress(progreso);
        }

        @Override
        protected void onPreExecute() {
            pDialog.setOnCancelListener(new DialogInterface.OnCancelListener(){
                @Override
                public void onCancel(DialogInterface dialog) {
                    MiTareaAsincronaDialog.this.cancel(true);
                }
            });
            pDialog.setProgress(0);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result) {
                pDialog.dismiss();
                Toast.makeText(MainActivity.this, "Tarea finalizada!",
                Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(MainActivity.this, "Tarea cancelada!",
                    Toast.LENGTH_SHORT).show();
        }

    }
}
